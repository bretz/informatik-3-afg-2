import icontract

#class returns kernel matrices list
class KernelFactory:

    @staticmethod
    def create_vertical_prewitt_kernel():
        return [-1, -1, -1, 0, 0, 0, 1, 1, 1]


    @staticmethod
    def create_horizontal_prewitt_kernel():
        return [-1, 0, 1, -1, 0, 1, -1, 0, 1]

    @staticmethod
    @icontract.require(lambda radius: isinstance(radius, int) is True, f"wrong datatype for radius provided")
    def create_box_filter(radius):
        dimension = ((2 * radius) + 1) ** 2

        return [1 / dimension for _ in range(dimension)]
