from Image import Image
from Kernel import Kernel
from KernelFactory import KernelFactory
from ZeroPaddingBorderBehavior import ZeroPaddingBorderBehavior
from ClampingBorderBehavior import ClampingPaddingBorderBehavior


def aufgabe1():
    d = Image()
    d.read_from_file("Bild1.pgm")


    zero_p_bb = ZeroPaddingBorderBehavior()
    clamping_p_bb = ClampingPaddingBorderBehavior()

    horizontal_pk = Kernel(KernelFactory().create_horizontal_prewitt_kernel())
    vertical_pk = Kernel(KernelFactory().create_vertical_prewitt_kernel())

    d.convolve(horizontal_pk, zero_p_bb, "./images/ZeroPadded/Ergebnis1-horizontal.pgm")
    d.convolve(horizontal_pk, clamping_p_bb, "./images/Clamped/Ergebnis1-horizontal.pgm")
    d.convolve(vertical_pk, zero_p_bb, "./images/ZeroPadded/Ergebnis1-vertical.pgm")
    d.convolve(vertical_pk, clamping_p_bb, "./images/Clamped/Ergebnis1-vertical.pgm")

def aufgabe2():
    zero_p_bb = ZeroPaddingBorderBehavior()
    clamping_p_bb = ClampingPaddingBorderBehavior()

    e = Image()
    e.read_from_file("Bild2.pgm")
    #create_box_filter takes radius, not dimension (dimension = radius*2 +1)
    boxfilter_3 = Kernel(KernelFactory().create_box_filter(1))
    boxfilter_11 = Kernel(KernelFactory().create_box_filter(5))
    boxfilter_27 = Kernel(KernelFactory().create_box_filter(13))
    e.convolve(boxfilter_3, zero_p_bb, "./images/ZeroPadded/Ergebnis2-3.pgm")
    e.convolve(boxfilter_3, clamping_p_bb, "./images/Clamped/Ergebnis2-3.pgm")
    e.convolve(boxfilter_11, zero_p_bb, "./images/ZeroPadded/Ergebnis2-11.pgm")
    e.convolve(boxfilter_11, clamping_p_bb, "./images/Clamped/Ergebnis2-11.pgm")
    e.convolve(boxfilter_27, zero_p_bb, "./images/ZeroPadded/Ergebnis2-27.pgm")
    e.convolve(boxfilter_27, clamping_p_bb, "./images/Clamped/Ergebnis2-27.pgm")


#bekannte Bugs: Beide Prewitt-Filter erzeugen links auf mittlerer Höhe ein Quadrat an weißen Pixeln, die schwarz bleiben sollten
#               Boxfilter verdunkelt das Bild

if __name__ == "__main__":
    aufgabe1()
    aufgabe2()
